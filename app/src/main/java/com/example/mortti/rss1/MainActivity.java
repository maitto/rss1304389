package com.example.mortti.rss1;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.content.DialogInterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.io.*;
import android.os.Handler;

import javax.security.auth.login.LoginException;

public class MainActivity extends AppCompatActivity implements Observer{
    private int x = 0;
    private int i = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {






        RssList.getInstance().registerObserver(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        final ListView myLV = (ListView)findViewById(R.id.list);
        myLV.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final FeedItem item = (FeedItem) (myLV.getItemAtPosition(position));
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getLink()));
                startActivity(browserIntent);
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {


        final FeedParser2 fp = new FeedParser2();
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem addf = menu.findItem(R.id.lisaam);




        addf.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem addf) {
                AlertDialog.Builder alertbuilder = new AlertDialog.Builder(MainActivity.this);
                alertbuilder.setTitle("Enter RSS URL");
                final EditText input = new EditText(MainActivity.this);
                alertbuilder.setView(input);
                alertbuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String rssLink = input.getText().toString();
                        Log.e("AAAAAAAAAAAAAAAAAAA", ""+id);
                        fp.setURL(rssLink);
                        Thread t = new Thread(fp);
                        t.start();
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        menu.add(itemcounterget(),itemcounterget(),itemcounterget(),RssList.getInstance().getfeedname());
                        itemcounterincrese();


                    }
                });
                alertbuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertbuilder.create();
                alertDialog.show();
                return true;
            }
        });


        return true;

    }






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        Log.e("AAAAAAAAAAAAAAAAAAA", ""+id);

        if (id >= 1){
            Log.e("BBBBBBBBBBBBBBBBBBBBBB","");
            idsetter(id - 1);

            RssList.getInstance().notifyall();


        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void update() {
        final ListView myLV = (ListView)findViewById(R.id.list);
        final AppCompatActivity ac = this;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                myLV.setAdapter(new FeedAdapter(ac, R.layout.row_layout, RssList.getInstance().getrsslist().get(idgetter()).getfeeditems()));


            }
        });
    }

    public int itemcounterget(){
        return i;
    }
    public void itemcounterincrese(){
        i++;
    }
    public void idsetter(int xx){
        x = xx;
    }
    public int idgetter(){
        return x;
    }


}
