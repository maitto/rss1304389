package com.example.mortti.rss1;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.InputStreamReader;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Mortti on 29.9.2015.
 */
public class FeedParser2 implements Runnable {
    FeedItem i = null;
    TheFeed f = null;
    String url = "http://www.hs.fi/uutiset/rss/";

    public FeedParser2(){
    }
    public String getURL(){
        return url;
    }
    public void setURL(String urli){
        this.url = urli;
    }

    public void run() {

        try {

            Log.e("try fp2 alku","");
            URL url = new URL(getURL());

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(false);
            XmlPullParser xpp = factory.newPullParser();





            xpp.setInput(new InputStreamReader(url.openStream()));

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                    Log.e("while alku","");
                if (eventType == XmlPullParser.START_TAG) {
                    Log.e("start tag alku", "");

                    if (xpp.getName().equalsIgnoreCase("channel")) {
                        f = new TheFeed();
                        Log.e("channel löytyy", "UUSI THEFEED");
                        eventType = xpp.next();
                        RssList.getInstance().setfeedname(xpp.nextText());

                    }
                    else if (xpp.getName().equalsIgnoreCase("item")) {
                        i = new FeedItem();
                        Log.e("UUSI FEEDITEM", "");
                        while(true) {

                            if (xpp.getName().equalsIgnoreCase("title")) {
                                //items.add(xpp.nextText());
                                Log.e("start tag, title", "SET TITLE");
                                i.setTitle(xpp.nextText());
                                break;

                            }
                            else{
                                eventType = xpp.next();
                            }
                        }
                            while(true) {
                                if (xpp.getName().equalsIgnoreCase("link")) {
                                    // Log.d("GREC", xpp.nextText());2
                                    //links.add(xpp.nextText());
                                    i.setLink(xpp.nextText());
                                    Log.e("start tag, link", "SET LINK");
                                    f.addfeeditem(i);
                                    break;


                                } else {
                                    eventType = xpp.next();
                                }
                            }
                    }




                }
                if (eventType == XmlPullParser.END_TAG) {
                    if (xpp.getName().equalsIgnoreCase("channel")) {
                        Log.e("LISTEIHIN LISÄILY", "");

                    }
                }

                eventType = xpp.next();
            }



            RssList.getInstance().rsslist.add(f);
            Log.e("rsslista koko", ""+RssList.getInstance().rsslist.size());
            RssList.getInstance().notifyall();
            Log.e("NOTIFY","");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
