package com.example.mortti.rss1;

import android.util.Log;

/**
 * Created by Mortti on 5.10.2015.
 */
public class FeedItem {

    private String title;
    private String link;


    public void setTitle(String title) {
        this.title = title;
        Log.e("E", title);
    }
    public void setLink(String link){
        this.link = link;
        Log.e("E", link);
    }
    public String getTitle() {
        return title;
    }
    public String getLink(){
        return link;
    }

}
