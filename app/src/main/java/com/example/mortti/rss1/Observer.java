package com.example.mortti.rss1;


/**
 * Created by Mortti on 5.10.2015.
 */
public interface Observer {
    public void update();
}
