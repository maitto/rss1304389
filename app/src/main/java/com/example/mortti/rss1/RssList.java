package com.example.mortti.rss1;
import java.util.ArrayList;

/**
 * Created by Mortti on 2.10.2015.
 */
public class RssList {
    private String name;
    private ArrayList<Observer> obs = new ArrayList<Observer>();
    public void registerObserver (Observer o){
        obs.add(o);
    }
    public void notifyall(){
        for(Observer observer:obs){
            observer.update();
        }
    }
    private static final RssList INSTANCE = new RssList();
    private RssList() {}
    public static RssList getInstance() {
        return INSTANCE;
    }
    ArrayList<TheFeed> rsslist = new ArrayList<TheFeed>();

    public ArrayList<TheFeed> getrsslist(){
    return rsslist;
    }

    public void setfeedname(String name){
        this.name=name;
    }
    public String getfeedname(){
        return name;
    }

}
