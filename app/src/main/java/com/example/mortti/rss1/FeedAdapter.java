package com.example.mortti.rss1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Mortti on 1.10.2015.
 */

public class FeedAdapter extends ArrayAdapter<FeedItem> {
    private ArrayList<FeedItem> feedItems;
    private Context context;

    public FeedAdapter(Context context, int resource, ArrayList<FeedItem> feedItems){
        super (context, resource, feedItems);
        this.feedItems = feedItems;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View targetView = convertView;
        if (targetView == null){
            LayoutInflater li = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            targetView = li.inflate(R.layout.row_layout, null);
        }

        FeedItem thisFeedItem = feedItems.get(position);
        if(thisFeedItem != null){
            TextView tv = (TextView)targetView.findViewById(R.id.item_title);
            tv.setText(thisFeedItem.getTitle());

        }
        return targetView;
    }

}
